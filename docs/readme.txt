Requirements:
- Soap must be compiled with PHP.  Contact your host.
- You will need to apply to FedEx for a production account: https://www.fedex.com/wpor/web/jsp/drclinks.jsp?links=techresources/production.html

Installation:

1. upload all files to your server
2. in your browser go to your Zen Cart ADMIN->MODULES->SHIPPING->FEDEX SMARTPOST and enable the module.
3. By default, a test key and meter number are provided.  Verify the module is working before replacing with your production key and meter number.
4. Replace the account number and password.
5. Replace the HubID with your assigned HubID.

Uninstall:
1. Go to ADMIN->MODULES->SHIPPING->FEDEX SMARTPOST and click REMOVE.
2. Remove all files uploaded during the installation from your server.