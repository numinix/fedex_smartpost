<?php
define('MODULE_SHIPPING_FEDEX_SMARTPOST_TEXT_TITLE', 'FedEx SmartPost');
define('MODULE_SHIPPING_FEDEX_SMARTPOST_TEXT_DESCRIPTION', 'Federal Express<br><br>You will need to have registered an account with FedEx and proper approval from FedEx identity to use this module. Please see the README.TXT file for other requirements.');
// eof