FedEx SmartPost Shipping Module for Zen Cart by Numinix (http://www.numinix.com).

Install:
1) Upload all files while maintaining the directory structure.
2) Go to ADMIN->MODULES->SHIPPING->FEDEX SMARTPOST and configure the module.

Note: you will need to request test and production server account information from FedEx.

Uninstall:
1) Go to ADMIN->MODULES->SHIPPING->FEDEX SMARTPOST and click "Remove".
2) Delete all of the included files from your server.